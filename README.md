```html
Client side code.
Basic library requirements:
1. Angular-1.2.2 (Tested).
2. SimpleWebrtc's latest.js (URL: http://simplewebrtc.com/latest.js).

If you are using default template provided by us the requirements are as follows:
1. ui-bootstrap-alert/ui-bootstrap-alert-0.10.0.js Not the entire ui-bootstrap library.
2. angular-strap version v2.0.0.


<video-chat  user="user" contacts="contacts" login-template-provided="true"  socket-url="" ></video-chat>
This library's directive needs the following attributes:
1. user - This attribute should contain object of the following format
    user = { name: 'Name', id: 'Id' }
    name: This is the display name.
    id: This should be a unique Id for the user.
2. contacts - This attribute should contain object of the following format
    contacts= [{name: "Name", id: 'Id', picture: 'image file', loginStatus: 'Offline', loginType: 'name of the channel used'}];
    name: This is the display name.
    id: This should be a unique Id for the user.
    picture: This contains the image file.
    loginStatus: This maintains the login status, by default put it as Offline.
    loginType: Name of the channel, eg. Facebook the source of the contacts 
               "The similar name image should be placed in /images/icons/ folder 
                if using the default template."
3. login-template-provided: If you have your own mechanism to populate user and 
                            the contacts then put true else put false because you 
                            would be relying on the videoChat to even create 
                            the user for you.
4. socket-url - If you have hosted your server on the server which server the js 
                files then you can keep it blank, else give the full URL of the server.


Server side code.
The server code is constituted of 2 files:
1. SocketConnection.js.
2. ClientSocketPool.js.

All you need to do is pass the socket io variable to the SocketConnection.js while requiring SocketConnection.js
Here is the sample code:

var socketConnection = require("./SocketConnection");
socketConnection(io);

where io is the socket io connection.

Thats it, you are good to go.
```
