var videoChat = angular.module('Video-Chat', [/*'ui.bootstrap'*/'mgcrea.ngStrap', 'ui.bootstrap.alert', 'VideoChatTemplates'])
.constant("VIDEO_CHAT_CONSTANTS", {
        template:"templates/VideoChat.html"
});

/* Directives */
/* Video chat directive */
videoChat.directive('videoChat',["VIDEO_CHAT_CONSTANTS", function(VIDEO_CHAT_CONSTANTS){
    return{
        restrict: 'E',
        scope:{
            user: '=user',
            contacts: '=contacts',
            loginTemplateProvided: '@',
            socketUrl: '@',
            videoChatTemplate: '@'
        },
        templateUrl: function($node, attrs){
            var template;
            if(attrs.videoChatTemplate == null){
                template = VIDEO_CHAT_CONSTANTS.template;
            } else{
                template = attrs.videoChatTemplate;
            }
            return template;
        },
        controller:'VideoChatController'
    }
}]);

/* Backgrounf Image */
angular.module('Video-Chat')
.directive('backgroundImage', function(){
    return {
            scope: {
                backgroundImage:'='
            },
            link: function(scope, element, attrs){
                var url = scope.backgroundImage;
                element.css({
                    'background-image': 'url(' + url +')'
                });
            }
        };
});

/* Caption video directive */
angular.module('Video-Chat')
.directive('captionVideo',['$rootScope', 
'$compile', 
'$location', 
'$anchorScroll', 
'Webrtc',  
function($rootScope, 
$compile, 
$location, 
$anchorScroll, 
Webrtc){
    return {
            compile: function(element, attrs){
            var captionTemplate = '<div class="video-caption">'
            /*+'<span class="glyphicon video-control glyphicon-facetime-video video-on" ng-class="{videoOff: videoOff}" ng-click="videoToggle()"></span>'
            +'<span class="glyphicon video-control glyphicon-pause pause-on" ng-class="{pauseOff: pauseOff}" ng-click="pauseToggle()"></span>'*/
            +'<span class="glyphicon video-control glyphicon-volume-off mute-on" ng-class="{muteOff: muteOff}" ng-click="muteToggle()"></span>'
            /*+'<span class="glyphicon video-control glyphicon-volume-down volume-down-on" ng-class="{volumeDownOff: volumeDownOff}" ng-click="volumeDown()"></span>'
            +'<span class="glyphicon video-control glyphicon-volume-up volume-up-on" ng-class="{volumeUpOff: volumeUpOff}" ng-click="volumeUp()"></span>'*/
            +'<span class="glyphicon video-control glyphicon-earphone end-call" ng-click="endCall()"></span>'
            +'</div>';
                var linkFunction = $compile(captionTemplate);
                return function(scope, element, attrs) {
                    var captionElement = linkFunction(scope);
                    element.append(captionElement);

                    scope.videoToggle = function(){
                        if(scope.videoOff){
                            Webrtc.stopVideo();
                        } else{
                            Webrtc.startVideo();
                        }
                        scope.videoOff = !scope.videoOff;
                    };
                    scope.muteToggle = function(){
                        if(scope.muteOff){
                            Webrtc.unmute();
                        } else{
                            Webrtc.mute();
                        }
                        scope.muteOff = !scope.muteOff;
                    };
                    scope.volumeDown = function(){
                        if(!scope.volumeDownOff){
                            scope.volumeDownOff = Webrtc.decreaseVolume();
                            if(!scope.volumeDownOff){
                                scope.volumeUpOff = false;
                            }
                        }
                    };
                    scope.volumeUp = function(){
                        if(!scope.volumeUpOff){
                            scope.volumeUpOff = Webrtc.increaseVolume();
                            if(!scope.volumeUpOff){
                                scope.volumeDownOff = false;
                            }
                        }
                    };
                    scope.endCall = function(){
                        scope.terminateCall();
                    }
                    scope.videoOff = false;
                    scope.pauseOff = false;
                    scope.muteOff = false;
                    scope.volumeDownOff = false;
                    scope.volumeUpOff = false;
                };
            }
        };
}]);
//<img id="mic" src="images/microphone.png" height="100%"  ><span ></span>

/* Container minimize directive */
angular.module('Video-Chat')
.directive('containerMinimize',['$rootScope', 
'$compile', 
function($rootScope, 
$compile){
    return {
        compile: function(element, attrs){
            return function(scope, element, attrs) {
                toggleClass = function(attribute){
                    $rootScope.$apply(function(){
                        if(attribute == 'local-video'){
                            scope.localVideoMinimize = !scope.localVideoMinimize;
                            scope.showLocalVideoIcon = !scope.showLocalVideoIcon;
                        } else if(attribute == 'remote-video'){
                            scope.remoteVideoMinimize = !scope.remoteVideoMinimize;
                            scope.showRemoteVideoIcon = !scope.showRemoteVideoIcon;
                        } else if(attribute == 'contacts'){
                            scope.contactsMinimize = !scope.contactsMinimize;
                            scope.showContactsIcon = !scope.showContactsIcon;
                        }
                    });
                }

                scope.$watch('callPartner', function(){
                    if(scope.callPartner.id != null){
                        scope.remoteVideoMinimize = false;
                    } else{
                        scope.remoteVideoMinimize = true;
                        scope.showRemoteVideoIcon = false;
                    }
                });

                element.bind('click', function(){
                    toggleClass(attrs.containerMinimize);
                });
                angular.element(document.getElementById("hide-"+attrs.containerMinimize+"-icon")).bind('click', function(){
                    toggleClass(attrs.containerMinimize);
                });
                scope.localVideoMinimize = false;
                scope.showLocalVideoIcon = false;
                scope.remoteVideoMinimize = true;
                scope.showRemoteVideoIcon = false;
                scope.remoteContactsMinimize = false;
                scope.showContactsIcon = false;
            }   
        }
    }
}]);

/* Draggable directive */
angular.module('Video-Chat')
.directive('draggable', ['$document', function($document) {
      return function(scope, element, attr) {
        var startX = 0, startY = 0, x = 0, y = 0;

        console.log('Started........');
        element.css({
         position: 'relative',
         cursor: 'pointer'
        });

        element.on('mousedown', function(event) {
          // Prevent default dragging of selected content
          event.preventDefault();
          startX = event.pageX - x;
          startY = event.pageY - y;
          $document.on('mousemove', mousemove);
          $document.on('mouseup', mouseup);
        });

        function mousemove(event) {
          y = event.pageY - startY;
          x = event.pageX - startX;
          var parentElement = element.parent();
          parentElement.css({
            top: y + 'px',
            left:  x + 'px'
          });
        }

        function mouseup() {
          $document.unbind('mousemove', mousemove);
          $document.unbind('mouseup', mouseup);
        }
      };
}]);

/* Controllers */
/* VideoChatController */
var videoChat = angular.module('Video-Chat');
videoChat.controller("VideoChatController", [
'$rootScope',
'$scope', 
'$attrs',
'$http', 
'$modal',
'$alert',
'Socket', 
'Webrtc',
function(
$rootScope,
$scope, 
$attrs, 
$http, 
$modal,
$alert,
Socket, 
Webrtc
){

    $scope.contacts = [];
    $scope.calls = [];
    var clientName; 
    $scope.registerDetails = {};
    $scope.callPartner = {name: null, id: null};

    var returnThen ={
                        then: function(callback){
                                  callback();
                        }
                    }

    connectSocket = function(socketUrl){
        if(socketUrl==""){
            socketUrl = null;
        }
        Socket.connect(socketUrl);
        return returnThen;
    }

    addSlashIfAbsent = function(contactsHostApi){
        if(contactsHostApi.substring(1,0) != '/'){
            //error in the video-chat directive regarding to the contacts-host-api / not present.
            //Slash is mandatory for nodejs options field.
            contactsHostApi = '/'+ contactsHostApi;
        }
        return contactsHostApi;
    }

    addDefaultPortIfNull = function(contactsHostPort){
        if(contactsHostPort == ""){
            contactsHostPort = 80;
        }
        return contactsHostPort;
    }

    addDefaultContactTypeIfNull = function(contactsType){
        if(contactsType == ""){
            contactsType = "All";
        }
        return contactsType;
    }

    checkContactsFor = function(searchForUser, callback){
        $scope.contacts.forEach(function(user){
            callback(searchForUser, user);
        }); 
    }

    $scope.$watch('onCallFriend', function(){
        console.log('onCallFriend has changed to: ', $scope.onCallFriend);
        if($scope.onCallFriend != null){
            checkContactsFor($scope.onCallFriend, function(friendId, user){
                if(friendId == user.id){
                    $scope.callPartner = user;
                    console.log('Call Partner: ', $scope.callPartner);
                }
            });
        }
    });

    $scope.$watch('user', function(newValue, oldValue){
        if($scope.user.name != null){
            console.log('user.name is ', $scope.user.name);
            bindSocket();
        } else{
            //Add Native template here........
        }
    }, true);

    bindTemplate = function(){
        var userDetailsLoaded;
        $attrs.$observe('loginTemplateProvided', function(loginTemplateProvided){
            try{
                loginTemplateProvided = JSON.parse(loginTemplateProvided);
                if(loginTemplateProvided){
                    console.log('Template provided......');
                } else{
                    console.log('Template not provided..........');
                    $scope.title = 'Login and Chat';
                    $scope.loginPopup = $modal({scope: $scope,
                                                template: 'templates/DefaultLoginTemplate.html',
                                                animation: "am-fade-X", 
                                                show: false, 
                                                backdrop: 'static', 
                                                keyboard: false});
                    $scope.loginPopup.$promise.then(function(){
                        $scope.$watch('user', function(newValue, oldValue){
                            if($scope.user.name != null){
                                userDetailsLoaded = true;
                            } else{
                                userDetailsLoaded = false;
                            }
                            toggleLoginPopup();
                        }, true);
                    });
 
                    toggleLoginPopup = function(){
                        if(userDetailsLoaded == true){
                            $scope.loginPopup.hide();
                        } else if(userDetailsLoaded == false){
                            //Add this under else if...........
                            $scope.loginPopup.show();
                        }
                    }
 
                    $scope.nativeLogin = function(){
                        $scope.user.name = $scope.registerDetails.name;
                        $scope.user.loginStatus = 'Online';
                        $scope.user.uniqueId = $scope.registerDetails.name;
                        $scope.user.loginType = 'Native';
                        $scope.user.picture = 'images/Male.jpg';
                    }
 
                }
            } catch(error){
                console.log('You have probably not provided Boolean variable in login-template-provided', error);
            }
        });
    }

    bindTemplate();

    bindSocket = function(){
        console.log('Entered bindsocket function');
        var clientDetails = $scope.user;
        $attrs.$observe('socketUrl', function(socketUrl){
            connectSocket(socketUrl).then(function(){
                socketBindings().then(function(){
                    Socket.emit('join', clientDetails);
                    //Socket.emit('getUsersList', clientDetails);
                    console.log('Watcher initiated');
                    var unbindContactsWatch = $scope.$watch('contacts', function(updatedContacts, oldContacts){
                        console.log('contacts watcher handler is triggered with (updatedContacts, oldContacts)....')
                        console.log(updatedContacts, oldContacts);
                        console.log('scope.contacts is: ', $scope.contacts);
                        if($scope.contacts.length > 0){
                             Socket.emit('registerFriends', $scope.contacts);
                             unbindContactsWatch();
                             //makes a cycle which continously keeps making this above call to the server.
                        }
                    });
                    $scope.$watch('user.loginStatus', function(newValue, oldValue){
                        if($scope.user.loginStatus == 'Logout'){
                            $scope.contacts.length = 0;
                            $scope.terminateCall();
                            Socket.emit('leave', clientDetails);
                        }
                    });
                });
            });
        }); 
    }

    $scope.terminateCall = function(){
        if(Webrtc.hasRoom()){
            console.log('Has Room: ', Webrtc.hasRoom());
            Socket.emit('endCall', {terminator: true});
        }
        stopCall(null);
    }

    stopCall = function(offlineUser){
        if(offlineUser == null || offlineUser.id == $scope.callPartner.id ){
            Webrtc.leaveRoom();
            $scope.onCallFriend = null;
            $scope.callPartner = {name: null, id: null};
        }
    }
    
    socketBindings = function(){

        Socket.on('addedNewUser', function (clientDetails) {
            console.log('Adding new user', clientDetails);
            var insertClient = clientDetails;
            $scope.contacts.filter(function(onlineUser){
                if(onlineUser.id == clientDetails.id){
                    //if same client has broadcasted do not add a new user but update loginStatus.
                    insertClient = null;
                    onlineUser.loginStatus = clientDetails.loginStatus;
                }
            });
            if(insertClient != null){
                $scope.contacts.push(insertClient);
            }
        });
   
        Socket.on('userOffline', function (clientDetails) {
            console.log('Removing user: ', clientDetails);
            $scope.contacts.forEach(function(user, userIndex) {
                if(user.id == clientDetails.id){
                    $scope.contacts[userIndex] = clientDetails;
                    stopCall(clientDetails);
                    //ending the call if the user has gone offline.
                }
            });
        });
   
        Socket.on('updatedFriendsList', function (contacts) {
            console.log('Updating.....');
            $scope.contacts = contacts;
        });
   
        Socket.on('friendUpdated', function (updatedFriend) {
            $scope.contacts.forEach(function(friendIndex, friendDetails){
                if(updatedFriend.id == friendDetails.id){
                        $scope.contacts[friendIndex] = updatedFriend;
                }
            });
        });

        //This party is the Callee..
        Socket.on('incomingCall', function(callDetails){
            var callee = callDetails.callee;
            var caller = callDetails.caller;
            var message = caller.name + "is calling."
            if($scope.calls.length > 0){
                var callCount = 0;
                $scope.calls.forEach(function(call){
                    callCount = callCount + 1;
                    if(call.id != caller.id && callCount == $scope.calls.length){
                        populateCallInformation(caller, "CALLER");
                    }
                });
            } else if($scope.calls.length == 4){
                //excess number of calls
            } else{
                populateCallInformation(caller, "CALLER");
            }
        });
   
        //This party is the Caller.
        Socket.on('acknowledgeCallee', function(answerDetails){
            console.log("Acknowledge");
            if(answerDetails.message.status == "picked"){
                $scope.onCallFriend = answerDetails.callee;
                console.log("Success");
                var roomName = answerDetails.message.roomName;
                console.log(roomName);
                Webrtc.joinRoom(roomName);
            } else {
                alert("Call rejected...!!");
            }
            dePopulateCallInformation(0);//Hack
        });
   
        Socket.on("ERROR", function(error) {
            alert(error.message);
        });

        Socket.on("leaveWebrtcRoom", function(call){
            console.log('Stop video chat');
            stopCall(null);
            if(call != null){
                Socket.emit('endCall', call);
            }
        })

        return returnThen;
    }

    checkIfCameraOnOperate = function(callbackOperation, argument1, argument2){
        if(!Webrtc.localVideoUp()){
            console.log('Local Video not up...');
            alert("Please allow usage of your camera.");
            Webrtc.startLocalVideo();
        }
    
        if(Webrtc.localVideoUp()){
            callbackOperation(argument1, argument2);
        }
    }

    $scope.initiateChat = function(user){
        checkIfCameraOnOperate(function(user){
            var userId = user.id;
            console.log("Calling");
            checkContactsFor(userId, function(userId, user){
                if(user.id == userId && user.loginStatus == 'Online'){
                    populateCallInformation(user, "CALLEE");
                    Socket.emit('callRequest', userId);
                } else if(user.id == userId && user.loginStatus == 'Offline'){
                    alert(user.name+' is offline...');
                }
            });
        }, user);
    }

    populateCallInformation = function(callParty, callPartyType){
       //Add a call notification to party
       if($scope.calls.length < 4){
           if($scope.calls.length == 0){
               callParty.type = 'success';
           } else{
               callParty.type = 'warning';
           }

	   if(callPartyType == "CALLER"){
               callParty.message = callParty.name + " is calling";
           } else{
               callParty.message = "Calling " + callParty.name + "\n Please wait...";
           }

           callParty.partyType = callPartyType;
           $scope.calls.push(callParty);
       } else{
          //Excess number of calls to a person.. 
       }
    }

    dePopulateCallInformation = function(index){
        $scope.calls.splice(index, 1);
    }

    $scope.rejectCall = function(index, callerId) {
        //$scope.calls.splice(index, 1);
        dePopulateCallInformation(index);
        console.log("Caller is: "+callerId);
        var callAnswerDetails = {
            callerId: callerId,
            answer:{ 
                     status: 'rejected'
            }
        }
        Socket.emit('callAnswer', callAnswerDetails);
    };

    $scope.answerCall = function(index, callerId){
        checkIfCameraOnOperate(function(index, callerId){
            $scope.onCallFriend = callerId;//Friend who is connected on the other side.
            console.log('On Call Friend: ', $scope.onCallFriend);
            //$scope.calls.splice(index, 1);
            dePopulateCallInformation(index);
            console.log("Answering...");
            var roomName = createRoomName(10);
            var callAnswerDetails = {
                callerId: callerId,
                answer:{ 
                         status: 'picked',
                         roomName: roomName
                }
            }
            Socket.emit('callAnswer', callAnswerDetails);
            console.log(roomName);
            Webrtc.joinRoom(roomName);
        } ,index, callerId);
    }

    createRoomName = function(nameSize){
        var roomName = "";
        var possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
        for( var i=0; i < nameSize; i++ )
        {
            roomName += possibleLetters.charAt(Math.floor(Math.random() * possibleLetters.length));
        }
        return roomName;
    }
}]);

/* Services */
/* Socket service */
var videoChat = angular.module('Video-Chat');
videoChat.factory('Socket', function ($rootScope) {
  var socket;
  return {
    connect: function(url) {
      socket = io.connect(url);
    },
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
});

/*Webrtc service */
var videoChat = angular.module('Video-Chat');
videoChat.factory('Webrtc', function ($rootScope) {
    var webrtc = new SimpleWebRTC({
        localVideoEl: 'local-video',
        autoRequestMedia: true,
        remoteVideosEl: 'remote-video'
    });
    var volume = 1;
    return {
        joinRoom: function(roomName){
            webrtc.joinRoom(roomName);
        },
        leaveRoom: function(){
            webrtc.leaveRoom();
        },
        hasRoom: function(){
            if(webrtc.roomName != undefined){
                return true;
            }
            return false;
        },
        mute: function(){
            webrtc.mute();
        },
        unmute: function(){
            webrtc.unmute();
        },
        stopVideo: function(){
            webrtc.stopLocalVideo();
        },
        startVideo: function(){
            webrtc.startLocalVideo();
        },
        increaseVolume: function(){
            var returnable;
            if(volume == 1){
                returnable = false;
            } else{
                volume = parseFloat(volume + 0.1);
                returnable = true;
            }
            if(returnable){
                this._setVolume(volume);
            }
            return returnable;
        },
        decreaseVolume: function(){
            var returnable;
            if(volume == 0){
                returnable = false;
            } else{
                volume = parseFloat(volume - 0.1);
                returnable = true;
            }
            if(returnable){
                this._setVolume(volume);
            }
            return returnable;
        },
        _setVolume: function(volume){
            webrtc.setVolumeForAll(volume);
        },
        localVideoUp: function(){
            if(webrtc.getLocalVideoContainer().src == ""){
              return false;
            } else{
              return true;
            }
        },
        startLocalVideo: function(){
            webrtc.startLocalVideo();
        }
    };
})

/* Templates */
/* VideoChatTemplate */
angular.module("VideoChatTemplates", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("templates/DefautLoginTemplate.html",
     '<div class="modal" tabindex="-1" role="dialog">' +
       '<div class="modal-dialog">' +
         '<div class="modal-content">' +
           '<div class="modal-header" ng-show="title">' +
             '<h4 class="modal-title" ng-bind="title"></h4>' +
           '</div>' +
           '<div class="modal-body" ng-switch on="loginPanelSelection">' +
               '<div class="row">' +
                   '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="padding: 20px 80px 20px 80px;">' +
                       '<input type="text" class="form-control custom-input" placeholder="Type in your name" ng-model="registerDetails.name">' +
                       '<button class="btn btn-secondary form-control custom-input" style="margin-top:30px" ng-click="nativeLogin()">SIGN-IN' +
                       '</button>' +
                   '</div>' +
               '</div>' +
           '</div>' +
         '</div>' +
       '</div>' +
     '</div>');

     $templateCache.put("templates/VideoChat.html", 
      '<style type="text/css">body{overflow-y:hidden}.image-container{width:80px;padding-right:0;padding-left:0}.container{margin-right:5px;margin-left:5px;width:auto}.badge{background-color:green}.page-header{padding:0}#local-video{width:100%}#remote-video{width:100%;height:auto;text-align:center;}.list-group{width:100%;overflow-y:auto;margin-bottom:0;max-height:40%;margin-top:5px;overflow-x:hidden}.friends-tab{padding:0px;width:auto;}.contacts-icon{display:none;width:20;position:absolute;top:0;}.Facebook{display:block}.Online:after{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px green;position:absolute;left:-11px;top:0}.Offline:after{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px grey;position:absolute;left:-11px;top:0}.Busy{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px #f80909;position:absolute;left:-11px;top:0}.page-header{margin:0 0 20px}.header{margin:0;margin-bottom:5;font-size:30}#footer{margin:0;position:absolute;width:100%;bottom:0;height:40px}.login-picture{width:50px}.header-row{margin:0}.account{float:right}.user-name{margin-top:14px;margin-right:5px}.user-row{margin-right:-10px;margin-left:5px;cursor: alias;}.notification{width:200px;position:fixed;bottom:0;right:0;margin-right:14px}.CALLER{display:block}.CALLEE{display:none}.search-box{margin-top:10px}.appear{display:none}.contactsImporter{position:fixed;right:0;top:40%;z-index:2}.glyphicon{font-size:15px;cursor:pointer;margin-top:3px;margin-bottom:0}.camera-icon{position:absolute;left:0;top:0}#local-video-container{position:relative;overflow:hidden;z-index:3;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.localVideoMinimize{position:fixed!important;left:-1000px!important}#hide-local-video-icon{cursor:pointer;position:fixed;z-index:5;left:-100px;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showLocalVideoIcon{left:0!important}#contacts-container{position:relative;overflow:hidden;z-index:2;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.contactsMinimize{position:fixed!important;left:-1000px!important}#hide-contacts-icon{cursor:pointer;position:fixed;z-index:5;left:-100px;top:50%;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showContactsIcon{left:0!important}' +
      '.remoteVideoMinimize{position:fixed!important;right:-3000px!important}#hide-remote-video-icon{cursor:pointer;position:fixed;z-index:5;right:-100px;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showRemoteVideoIcon{right:0!important}.close{opacity:1}.video-caption{background:#aaa8a8;width:50px;border-radius:5px;padding:5px;opacity:1;margin:0px auto;margin-top:-25px;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out} #remote-video-container{position:relative; -webkit-transition: all .7s ease-out;-moz-transition: all .7s ease-out;-ms-transition: all .7s ease-out;-o-transition: all .7s ease-out;transition: all .7s ease-out;} #remote-video-container:hover .video-caption{top:95%}.video-control{font-size:20;top:7%;margin-top:0;margin-bottom:0}.video-on{opacity:1;cursor:pointer;color:#000}.videoOff{opacity:.5!important;color:red}.pause-on{opacity:1;cursor:pointer;color:#000}.pauseOff{opacity:.5!important;color:red}.mute-on{opacity:1;cursor:pointer;color:#000}.muteOff{opacity:.5!important;color:red}.volume-up-on{opacity:1;cursor:pointer;color:#000}.volumeUpOff{opacity:.5!important;color:red}.volume-down-on{opacity:1;cursor:pointer;color:#000}.volumeDownOff{opacity:.5!important;color:red}' +
      '.initiate-call{position: absolute;top: 5px;right: 0px;}.list-group-item{cursor:alias;}.call-Online{cursor:pointer;color:green}.call-Offline{cursor:default;}' +
      '.contact-thumbnail{display: block;border-radius: 4px;-webkit-transition: all 0.2s ease-in-out;transition: all 0.2s ease-in-out;padding:0px;}' +
      '@media(max-width: 264px){.contact-name{padding-left:0px;padding-right:0px;}}' +
      '@media(min-width: 264px){.contact-name{padding-left:15px;padding-right:0px;}}' +
      '@media(min-width: 760px){.contact-name{padding-left:0px;padding-right:0px;}}' +
      '@media(min-width: 929px){.contact-name{padding-left:15px;padding-right:0px;}}' +
      '</style>' +
      '<div id="hide-local-video-icon" ng-class="{showLocalVideoIcon:showLocalVideoIcon}">' +
          '<img ng-src="{{user.picture}}" class="img-thumbnail" >' +
          '<span class="glyphicon glyphicon-facetime-video camera-icon"></span>' +
      '</div>' +
      '<div id="hide-contacts-icon" ng-class="{showContactsIcon:showContactsIcon}">' +
          '<span class="glyphicon glyphicon-list-alt"></span>' +
      '</div>' +
      '<div id="hide-remote-video-icon" ng-class="{showRemoteVideoIcon:showRemoteVideoIcon}">' +
          '<img ng-src="{{callPartner.picture}}" class="img-thumbnail" >' +
          '<span class="glyphicon glyphicon-facetime-video camera-icon"></span>' +
      '</div>' +
      '<div class="container">' +
          '<div class="col-sm-3 col-md-3 col-lg-3" >' +
              '<div id="local-video-container" ng-class="{localVideoMinimize:localVideoMinimize}">' +
                  '<button container-minimize="local-video" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<video id="local-video" autoplay muted></video>' +
              '</div>' +
              '<div id="contacts-container" ng-class="{contactsMinimize:contactsMinimize}">' +
                  '<button container-minimize="contacts" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<input type="text" class="search-box form-control" ng-model="friendName" placeholder="Type in friend\'s name...">' +
                  '<div class="list-group" >' +
                      '<a href="#" ng-class="contact.loginStatus" class="list-group-item"' +
                       'ng-repeat="contact in contacts | filter: friendName | orderBy:\'loginStatus\':true">' +
                          '<div class="user-row row">' +
                              '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 friends-tab">' +
                                 '<img class="contact-thumbnail" ng-src="{{contact.picture}}">' +
                                 '<img ng-class="contact.loginType" ng-src="/images/icons/{{contact.loginType}}.png" class="contacts-icon">' +
                              '</div>' +
                             '<label class="col-xs-8 col-sm-8 col-md-8 col-lg-8 contact-name">{{contact.name}}</label>' +
                          '</div>' +
                          '<span class="glyphicon glyphicon-earphone initiate-call" ng-class="\'call-\' + contact.loginStatus"' +
                          ' ng-click="initiateChat(contact)"></span>' +
                      '</a>' +
                  '</div>' +
              '</div>' +
          '</div>' +
          '<div class=" col-sm-6 col-md-6 col-lg-6">' +
              '<div id="remote-video-container" ng-class="{remoteVideoMinimize:remoteVideoMinimize}" caption-video="remote">' +
                  '<button container-minimize="remote-video" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<div id="remote-video"></div>' +
              '</div>' +
              '<div class="notification">' +
                  '<alert ng-repeat="call in calls" type="call.type" close="rejectCall($index, call.id);">' +
                      '<center>{{call.message}}</center>' +
                      '<div class="row">' +
                          '<span class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-6 col-sm-6 col-md-6 col-lg-6 glyphicon glyphicon-earphone" ng-class="call.partyType"' +
                          ' ng-click="answerCall($index,call.id)" ></span>' +
                          '<span class="col-xm-6 col-sm-6 col-md-6 col-lg-6 glyphicon glyphicon-remove" ng-click="rejectCall($index, call.id)"></span>' +
                      '</div>' +
                  '</alert>' +
              '</div>' +
          '</div>' +
      '</div>'); 
}]);
