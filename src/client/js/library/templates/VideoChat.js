angular.module("VideoChatTemplates", []).run(["$templateCache", function($templateCache) {
    $templateCache.put("templates/DefautLoginTemplate.html",
     '<div class="modal" tabindex="-1" role="dialog">' +
       '<div class="modal-dialog">' +
         '<div class="modal-content">' +
           '<div class="modal-header" ng-show="title">' +
             '<h4 class="modal-title" ng-bind="title"></h4>' +
           '</div>' +
           '<div class="modal-body" ng-switch on="loginPanelSelection">' +
               '<div class="row">' +
                   '<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 " style="padding: 20px 80px 20px 80px;">' +
                       '<input type="text" class="form-control custom-input" placeholder="Type in your name" ng-model="registerDetails.name">' +
                       '<button class="btn btn-secondary form-control custom-input" style="margin-top:30px" ng-click="nativeLogin()">SIGN-IN' +
                       '</button>' +
                   '</div>' +
               '</div>' +
           '</div>' +
         '</div>' +
       '</div>' +
     '</div>');

     $templateCache.put("templates/VideoChat.html", 
      '<style type="text/css">body{overflow-y:hidden}.image-container{width:80px;padding-right:0;padding-left:0}.container{margin-right:5px;margin-left:5px;width:auto}.badge{background-color:green}.page-header{padding:0}#local-video{width:100%}#remote-video{width:100%;height:auto;text-align:center;}.list-group{width:100%;overflow-y:auto;margin-bottom:0;max-height:40%;margin-top:5px;overflow-x:hidden}.friends-tab{background-repeat:no-repeat;height:60px;background-size:100%;background-position:top}.contacts-icon{display:none;width:20;position:absolute;top:0;right:0}.Facebook{display:block}.Online:after{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px green;position:absolute;left:-11px;top:0}.Offline:after{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px grey;position:absolute;left:-11px;top:0}.Busy{content:"";display:block;margin-left:1px;height:100%;width:10px;box-shadow:10px 0 10px #f80909;position:absolute;left:-11px;top:0}.page-header{margin:0 0 20px}.header{margin:0;margin-bottom:5;font-size:30}#footer{margin:0;position:absolute;width:100%;bottom:0;height:40px}.login-picture{width:50px}.header-row{margin:0}.account{float:right}.user-name{margin-top:14px;margin-right:5px}.user-row{margin-right:-10px;margin-left:5px;cursor: alias;}.notification{width:200px;position:fixed;bottom:0;right:0;margin-right:14px}.CALLER{display:block}.CALLEE{display:none}.search-box{margin-top:10px}.appear{display:none}.contactsImporter{position:fixed;right:0;top:40%;z-index:2}.glyphicon{font-size:15px;cursor:pointer;margin-top:3px;margin-bottom:0}.camera-icon{position:absolute;left:0;top:0}#local-video-container{position:relative;overflow:hidden;z-index:3;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.localVideoMinimize{position:fixed!important;left:-1000px!important}#hide-local-video-icon{cursor:pointer;position:fixed;z-index:5;left:-100px;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showLocalVideoIcon{left:0!important}#contacts-container{position:relative;overflow:hidden;z-index:2;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.contactsMinimize{position:fixed!important;left:-1000px!important}#hide-contacts-icon{cursor:pointer;position:fixed;z-index:5;left:-100px;top:50%;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showContactsIcon{left:0!important}' +
      '.remoteVideoMinimize{position:fixed!important;right:-3000px!important}#hide-remote-video-icon{cursor:pointer;position:fixed;z-index:5;right:-100px;width:3%;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}.showRemoteVideoIcon{right:0!important}.close{opacity:1}.video-caption{background:#aaa8a8;width:50px;border-radius:5px;padding:5px;margin:0px auto;margin-top:-25px;opacity:1;-webkit-transition:all .7s ease-out;-moz-transition:all .7s ease-out;-ms-transition:all .7s ease-out;-o-transition:all .7s ease-out;transition:all .7s ease-out}#remote-video-container{position:relative; -webkit-transition: all .7s ease-out;-moz-transition: all .7s ease-out;-ms-transition: all .7s ease-out;-o-transition: all .7s ease-out;transition: all .7s ease-out;}#remote-video-container:hover .video-caption{top:95%}.video-control{font-size:20;top:7%;margin-top:0;margin-bottom:0}.video-on{opacity:1;cursor:pointer;color:#000}.videoOff{opacity:.5!important;color:red}.pause-on{opacity:1;cursor:pointer;color:#000}.pauseOff{opacity:.5!important;color:red}.mute-on{opacity:1;cursor:pointer;color:#000}.muteOff{opacity:.5!important;color:red}.volume-up-on{opacity:1;cursor:pointer;color:#000}.volumeUpOff{opacity:.5!important;color:red}.volume-down-on{opacity:1;cursor:pointer;color:#000}.volumeDownOff{opacity:.5!important;color:red}' +
      '.initiate-call{position: absolute;top: 5px;right: 0px;}.list-group-item{cursor:alias;}.call-Online{cursor:pointer;color:green}.call-Offline{cursor:default;}' +
      '</style>' +
      '<div id="hide-local-video-icon" ng-class="{showLocalVideoIcon:showLocalVideoIcon}">' +
          '<img ng-src="{{user.picture}}" class="img-thumbnail" >' +
          '<span class="glyphicon glyphicon-facetime-video camera-icon"></span>' +
      '</div>' +
      '<div id="hide-contacts-icon" ng-class="{showContactsIcon:showContactsIcon}">' +
          '<img src="images/icons/contacts.png" class="img-thumbnail">' +
      '</div>' +
      '<div id="hide-remote-video-icon" ng-class="{showRemoteVideoIcon:showRemoteVideoIcon}">' +
          '<img ng-src="{{callPartner.picture}}" class="img-thumbnail" >' +
          '<span class="glyphicon glyphicon-facetime-video camera-icon"></span>' +
      '</div>' +
      '<div class="container">' +
          '<div class="col-sm-3 col-md-3 col-lg-3" >' +
              '<div id="local-video-container" ng-class="{localVideoMinimize:localVideoMinimize}">' +
                  '<button container-minimize="local-video" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<video id="local-video" autoplay muted></video>' +
              '</div>' +
              '<div id="contacts-container" ng-class="{contactsMinimize:contactsMinimize}">' +
                  '<button container-minimize="contacts" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<input type="text" class="search-box form-control" ng-model="friendName" placeholder="Type in friend\'s name...">' +
                  '<div class="list-group" >' +
                      '<a href="#" ng-class="contact.loginStatus" class="list-group-item"' +
                       'ng-repeat="contact in contacts | filter: friendName | orderBy:\'loginStatus\':true">' +
                          '<div class="user-row row">' +
                              '<div class="col-xs-4 col-sm-4 col-md-4 col-lg-4 friends-tab" background-image="contact.picture">' +
                                 '<img ng-class="contact.loginType" ng-src="/images/icons/{{contact.loginType}}.png" class="contacts-icon">' +
                              '</div>' +
                             '<label class="col-xs-8 col-sm-8 col-md-8 col-lg-8">{{contact.name}}</label>' +
                          '</div>' +
                          '<span class="glyphicon glyphicon-earphone initiate-call" ng-class="\'call-\' + contact.loginStatus"' +
                          ' ng-click="initiateChat(contact)"></span>' +
                      '</a>' +
                  '</div>' +
              '</div>' +
          '</div>' +
          '<div class="col-sm-6 col-md-6 col-lg-6">' +
              '<div id="remote-video-container" ng-class="{remoteVideoMinimize:remoteVideoMinimize}" caption-video="remote">' +
                  '<button container-minimize="remote-video" type="button" class="close">&times;</button>' +
                  '<span draggable class="glyphicon glyphicon-move pull-right"></span>' +
                  '<div id="remote-video"></div>' +
              '</div>' +
              '<div class="notification">' +
                  '<alert ng-repeat="call in calls" type="call.type" close="rejectCall($index, call.id);">' +
                      '<center>{{call.message}}</center>' +
                      '<div class="row">' +
                          '<span class="col-xs-offset-3 col-sm-offset-3 col-md-offset-3 col-lg-offset-3 col-xs-6 col-sm-6 col-md-6 col-lg-6 glyphicon glyphicon-earphone" ng-class="call.partyType"' +
                          ' ng-click="answerCall($index,call.id)" ></span>' +
                          '<span class="col-xm-6 col-sm-6 col-md-6 col-lg-6 glyphicon glyphicon-remove" ng-click="rejectCall($index, call.id)"></span>' +
                      '</div>' +
                  '</alert>' +
              '</div>' +
          '</div>' +
      '</div>'); 
}]);
