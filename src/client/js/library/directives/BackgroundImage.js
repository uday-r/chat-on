angular.module('Video-Chat')
.directive('backgroundImage', function(){
    return {
            scope: {
                backgroundImage:'='
            },
            link: function(scope, element, attrs){
                var url = scope.backgroundImage;
                element.css({
                    'background-image': 'url(' + url +')'
                });
            }
        };
});
