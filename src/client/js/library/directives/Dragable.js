angular.module('Video-Chat')
.directive('draggable', ['$document', function($document) {
      return function(scope, element, attr) {
        var startX = 0, startY = 0, x = 0, y = 0;

        console.log('Started........');
        element.css({
         position: 'relative',
         cursor: 'pointer'
        });

        element.on('mousedown', function(event) {
          // Prevent default dragging of selected content
          event.preventDefault();
          startX = event.pageX - x;
          startY = event.pageY - y;
          $document.on('mousemove', mousemove);
          $document.on('mouseup', mouseup);
        });

        function mousemove(event) {
          y = event.pageY - startY;
          x = event.pageX - startX;
          var parentElement = element.parent();
          parentElement.css({
            top: y + 'px',
            left:  x + 'px'
          });
        }

        function mouseup() {
          $document.unbind('mousemove', mousemove);
          $document.unbind('mouseup', mouseup);
        }
      };
    }]);
