angular.module('Video-Chat')
.directive('containerMinimize',['$rootScope', 
'$compile', 
function($rootScope, 
$compile){
    return {
        compile: function(element, attrs){
            return function(scope, element, attrs) {
                toggleClass = function(attribute){
                    $rootScope.$apply(function(){
                        if(attribute == 'local-video'){
                            scope.localVideoMinimize = !scope.localVideoMinimize;
                            scope.showLocalVideoIcon = !scope.showLocalVideoIcon;
                        } else if(attribute == 'remote-video'){
                            scope.remoteVideoMinimize = !scope.remoteVideoMinimize;
                            scope.showRemoteVideoIcon = !scope.showRemoteVideoIcon;
                        } else if(attribute == 'contacts'){
                            scope.contactsMinimize = !scope.contactsMinimize;
                            scope.showContactsIcon = !scope.showContactsIcon;
                        }
                    });
                }

                scope.$watch('callPartner', function(){
                    if(scope.callPartner.id != null){
                        scope.remoteVideoMinimize = false;
                    } else{
                        scope.remoteVideoMinimize = true;
                        scope.showRemoteVideoIcon = false;
                    }
                });

                element.bind('click', function(){
                    toggleClass(attrs.containerMinimize);
                });
                angular.element(document.getElementById("hide-"+attrs.containerMinimize+"-icon")).bind('click', function(){
                    toggleClass(attrs.containerMinimize);
                });
                scope.localVideoMinimize = false;
                scope.showLocalVideoIcon = false;
                scope.remoteVideoMinimize = true;
                scope.showRemoteVideoIcon = false;
                scope.remoteContactsMinimize = false;
                scope.showContactsIcon = false;
            }   
        }
    }
}]);
