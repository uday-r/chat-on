angular.module('Video-Chat')
.directive('captionVideo',['$rootScope', 
'$compile', 
'$location', 
'$anchorScroll', 
'Webrtc',  
function($rootScope, 
$compile, 
$location, 
$anchorScroll, 
Webrtc){
    return {
            compile: function(element, attrs){
            var captionTemplate = '<div class="video-caption">'
            /*+'<span class="glyphicon video-control glyphicon-facetime-video video-on" ng-class="{videoOff: videoOff}" ng-click="videoToggle()"></span>'
            +'<span class="glyphicon video-control glyphicon-pause pause-on" ng-class="{pauseOff: pauseOff}" ng-click="pauseToggle()"></span>'*/
            +'<span class="glyphicon video-control glyphicon-volume-off mute-on" ng-class="{muteOff: muteOff}" ng-click="muteToggle()"></span>'
            /*+'<span class="glyphicon video-control glyphicon-volume-down volume-down-on" ng-class="{volumeDownOff: volumeDownOff}" ng-click="volumeDown()"></span>'
            +'<span class="glyphicon video-control glyphicon-volume-up volume-up-on" ng-class="{volumeUpOff: volumeUpOff}" ng-click="volumeUp()"></span>'*/
            +'<span class="glyphicon video-control glyphicon-earphone" ng-click="endCall()"></span>'
            +'</div>';
                var linkFunction = $compile(captionTemplate);
                return function(scope, element, attrs) {
                    var captionElement = linkFunction(scope);
                    element.append(captionElement);

                    scope.videoToggle = function(){
                        if(scope.videoOff){
                            Webrtc.stopVideo();
                        } else{
                            Webrtc.startVideo();
                        }
                        scope.videoOff = !scope.videoOff;
                    };
                    scope.muteToggle = function(){
                        if(scope.muteOff){
                            Webrtc.unmute();
                        } else{
                            Webrtc.mute();
                        }
                        scope.muteOff = !scope.muteOff;
                    };
                    scope.volumeDown = function(){
                        if(!scope.volumeDownOff){
                            scope.volumeDownOff = Webrtc.decreaseVolume();
                            if(!scope.volumeDownOff){
                                scope.volumeUpOff = false;
                            }
                        }
                    };
                    scope.volumeUp = function(){
                        if(!scope.volumeUpOff){
                            scope.volumeUpOff = Webrtc.increaseVolume();
                            if(!scope.volumeUpOff){
                                scope.volumeDownOff = false;
                            }
                        }
                    };
                    scope.endCall = function(){
                        scope.terminateCall();
                    }
                    scope.videoOff = false;
                    scope.pauseOff = false;
                    scope.muteOff = false;
                    scope.volumeDownOff = false;
                    scope.volumeUpOff = false;
                };
            }
        };
}]);
//<img id="mic" src="images/microphone.png" height="100%"  ><span ></span>
