var videoChat = angular.module('Video-Chat', [/*'ui.bootstrap'*/'mgcrea.ngStrap', 'ui.bootstrap.alert', 'VideoChatTemplates'])
.constant("VIDEO_CHAT_CONSTANTS", {
        template:"templates/VideoChat.html"
});
videoChat.directive('videoChat',["VIDEO_CHAT_CONSTANTS", function(VIDEO_CHAT_CONSTANTS){
    return{
        restrict: 'E',
        scope:{
            user: '=user',
            contacts: '=contacts',
            loginTemplateProvided: '@',
            socketUrl: '@',
            videoChatTemplate: '@'
        },
        templateUrl: function($node, attrs){
            var template;
            if(attrs.videoChatTemplate == null){
                template = VIDEO_CHAT_CONSTANTS.template;
            } else{
                template = attrs.videoChatTemplate;
            }
            return template;
        },
        controller:'VideoChatController'
    }
}]);
