var videoChat = angular.module('Video-Chat');
videoChat.factory('Webrtc', function ($rootScope) {
    var webrtc = new SimpleWebRTC({
        localVideoEl: 'local-video',
        autoRequestMedia: true,
        remoteVideosEl: 'remote-video'
    });
    var volume = 1;
    return {
        joinRoom: function(roomName){
            webrtc.joinRoom(roomName);
        },
        leaveRoom: function(){
            webrtc.leaveRoom();
        },
        hasRoom: function(){
            if(webrtc.roomName != undefined){
                return true;
            }
            return false;
        },
        mute: function(){
            webrtc.mute();
        },
        unmute: function(){
            webrtc.unmute();
        },
        stopVideo: function(){
            webrtc.stopLocalVideo();
        },
        startVideo: function(){
            webrtc.startLocalVideo();
        },
        increaseVolume: function(){
            var returnable;
            if(volume == 1){
                returnable = false;
            } else{
                volume = parseFloat(volume + 0.1);
                returnable = true;
            }
            if(returnable){
                this._setVolume(volume);
            }
            return returnable;
        },
        decreaseVolume: function(){
            var returnable;
            if(volume == 0){
                returnable = false;
            } else{
                volume = parseFloat(volume - 0.1);
                returnable = true;
            }
            if(returnable){
                this._setVolume(volume);
            }
            return returnable;
        },
        _setVolume: function(volume){
            webrtc.setVolumeForAll(volume);
        },
        localVideoUp: function(){
            if(webrtc.getLocalVideoContainer().src == ""){
              return false;
            } else{
              return true;
            }
        },
        startLocalVideo: function(){
            webrtc.startLocalVideo();
        }
    };
})
