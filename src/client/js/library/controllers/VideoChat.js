var videoChat = angular.module('Video-Chat');
videoChat.controller("VideoChatController", [
'$rootScope',
'$scope', 
'$attrs',
'$http', 
'$modal',
'$alert',
'Socket', 
'Webrtc',
function(
$rootScope,
$scope, 
$attrs, 
$http, 
$modal,
$alert,
Socket, 
Webrtc
){

    $scope.contacts = [];
    $scope.calls = [];
    var clientName; 
    $scope.registerDetails = {};
    $scope.callPartner = {name: null, id: null};

    var returnThen ={
                        then: function(callback){
                                  callback();
                        }
                    }

    connectSocket = function(socketUrl){
        if(socketUrl==""){
            socketUrl = null;
        }
        Socket.connect(socketUrl);
        return returnThen;
    }

    addSlashIfAbsent = function(contactsHostApi){
        if(contactsHostApi.substring(1,0) != '/'){
            //error in the video-chat directive regarding to the contacts-host-api / not present.
            //Slash is mandatory for nodejs options field.
            contactsHostApi = '/'+ contactsHostApi;
        }
        return contactsHostApi;
    }

    addDefaultPortIfNull = function(contactsHostPort){
        if(contactsHostPort == ""){
            contactsHostPort = 80;
        }
        return contactsHostPort;
    }

    addDefaultContactTypeIfNull = function(contactsType){
        if(contactsType == ""){
            contactsType = "All";
        }
        return contactsType;
    }

    checkContactsFor = function(searchForUser, callback){
        $scope.contacts.forEach(function(user){
            callback(searchForUser, user);
        }); 
    }

    $scope.$watch('onCallFriend', function(){
        console.log('onCallFriend has changed to: ', $scope.onCallFriend);
        if($scope.onCallFriend != null){
            checkContactsFor($scope.onCallFriend, function(friendId, user){
                if(friendId == user.id){
                    $scope.callPartner = user;
                    console.log('Call Partner: ', $scope.callPartner);
                }
            });
        }
    });

    $scope.$watch('user', function(newValue, oldValue){
        if($scope.user.name != null){
            console.log('user.name is ', $scope.user.name);
            bindSocket();
        } else{
            //Add Native template here........
        }
    }, true);

    bindTemplate = function(){
        var userDetailsLoaded;
        $attrs.$observe('loginTemplateProvided', function(loginTemplateProvided){
            try{
                loginTemplateProvided = JSON.parse(loginTemplateProvided);
                if(loginTemplateProvided){
                    console.log('Template provided......');
                } else{
                    console.log('Template not provided..........');
                    $scope.title = 'Login and Chat';
                    $scope.loginPopup = $modal({scope: $scope,
                                                template: 'templates/DefaultLoginTemplate.html',
                                                animation: "am-fade-X", 
                                                show: false, 
                                                backdrop: 'static', 
                                                keyboard: false});
                    $scope.loginPopup.$promise.then(function(){
                        $scope.$watch('user', function(newValue, oldValue){
                            if($scope.user.name != null){
                                userDetailsLoaded = true;
                            } else{
                                userDetailsLoaded = false;
                            }
                            toggleLoginPopup();
                        }, true);
                    });
 
                    toggleLoginPopup = function(){
                        if(userDetailsLoaded == true){
                            $scope.loginPopup.hide();
                        } else if(userDetailsLoaded == false){
                            //Add this under else if...........
                            $scope.loginPopup.show();
                        }
                    }
 
                    $scope.nativeLogin = function(){
                        $scope.user.name = $scope.registerDetails.name;
                        $scope.user.loginStatus = 'Online';
                        $scope.user.uniqueId = $scope.registerDetails.name;
                        $scope.user.loginType = 'Native';
                        $scope.user.picture = 'images/Male.jpg';
                    }
 
                }
            } catch(error){
                console.log('You have probably not provided Boolean variable in login-template-provided', error);
            }
        });
    }

    bindTemplate();

    bindSocket = function(){
        console.log('Entered bindsocket function');
        var clientDetails = $scope.user;
        $attrs.$observe('socketUrl', function(socketUrl){
            connectSocket(socketUrl).then(function(){
                socketBindings().then(function(){
                    Socket.emit('join', clientDetails);
                    //Socket.emit('getUsersList', clientDetails);
                    console.log('Watcher initiated');
                    var unbindContactsWatch = $scope.$watch('contacts', function(updatedContacts, oldContacts){
                        console.log('contacts watcher handler is triggered with (updatedContacts, oldContacts)....')
                        console.log(updatedContacts, oldContacts);
                        console.log('scope.contacts is: ', $scope.contacts);
                        if($scope.contacts.length > 0){
                             Socket.emit('registerFriends', $scope.contacts);
                             unbindContactsWatch();
                             //makes a cycle which continously keeps making this above call to the server.
                        }
                    });
                    $scope.$watch('user.loginStatus', function(newValue, oldValue){
                        if($scope.user.loginStatus == 'Logout'){
                            $scope.contacts.length = 0;
                            $scope.terminateCall();
                            Socket.emit('leave', clientDetails);
                        }
                    });
                });
            });
        }); 
    }

    $scope.terminateCall = function(){
        if(Webrtc.hasRoom()){
            console.log('Has Room: ', Webrtc.hasRoom());
            Socket.emit('endCall', {terminator: true});
        }
        stopCall(null);
    }

    stopCall = function(offlineUser){
        if(offlineUser == null || offlineUser.id == $scope.callPartner.id ){
            Webrtc.leaveRoom();
            $scope.onCallFriend = null;
            $scope.callPartner = {name: null, id: null};
        }
    }
    
    socketBindings = function(){

        Socket.on('addedNewUser', function (clientDetails) {
            console.log('Adding new user', clientDetails);
            var insertClient = clientDetails;
            $scope.contacts.filter(function(onlineUser){
                if(onlineUser.id == clientDetails.id){
                    //if same client has broadcasted do not add a new user but update loginStatus.
                    insertClient = null;
                    onlineUser.loginStatus = clientDetails.loginStatus;
                }
            });
            if(insertClient != null){
                $scope.contacts.push(insertClient);
            }
        });
   
        Socket.on('userOffline', function (clientDetails) {
            console.log('Removing user: ', clientDetails);
            $scope.contacts.forEach(function(user, userIndex) {
                if(user.id == clientDetails.id){
                    $scope.contacts[userIndex] = clientDetails;
                    stopCall(clientDetails);
                    //ending the call if the user has gone offline.
                }
            });
        });
   
        Socket.on('updatedFriendsList', function (contacts) {
            $scope.contacts = contacts;
        });
   
        Socket.on('friendUpdated', function (updatedFriend) {
            $scope.contacts.forEach(function(friendIndex, friendDetails){
                if(updatedFriend.id == friendDetails.id){
                        $scope.contacts[friendIndex] = updatedFriend;
                }
            });
        });

        //This party is the Callee..
        Socket.on('incomingCall', function(callDetails){
            var callee = callDetails.callee;
            var caller = callDetails.caller;
            var message = caller.name + "is calling."
            if($scope.calls.length > 0){
                var callCount = 0;
                $scope.calls.forEach(function(call){
                    callCount = callCount + 1;
                    if(call.id != caller.id && callCount == $scope.calls.length){
                        populateCallInformation(caller, "CALLER");
                    }
                });
            } else if($scope.calls.length == 4){
                //excess number of calls
            } else{
                populateCallInformation(caller, "CALLER");
            }
        });
   
        //This party is the Caller.
        Socket.on('acknowledgeCallee', function(answerDetails){
            console.log("Acknowledge");
            if(answerDetails.message.status == "picked"){
                $scope.onCallFriend = answerDetails.callee;
                console.log("Success");
                var roomName = answerDetails.message.roomName;
                console.log(roomName);
                Webrtc.joinRoom(roomName);
            } else {
                alert("Call rejected...!!");
            }
            dePopulateCallInformation(0);//Hack
        });
   
        Socket.on("ERROR", function(error) {
            alert(error.message);
        });

        Socket.on("leaveWebrtcRoom", function(call){
            console.log('Stop video chat');
            stopCall(null);
            if(call != null){
                Socket.emit('endCall', call);
            }
        })

        return returnThen;
    }

    checkIfCameraOnOperate = function(callbackOperation, argument1, argument2){
        if(!Webrtc.localVideoUp()){
            console.log('Local Video not up...');
            alert("Please allow usage of your camera.");
            Webrtc.startLocalVideo();
        }
    
        if(Webrtc.localVideoUp()){
            callbackOperation(argument1, argument2);
        }
    }

    $scope.initiateChat = function(user){
        checkIfCameraOnOperate(function(user){
            var userId = user.id;
            console.log("Calling");
            checkContactsFor(userId, function(userId, user){
                if(user.id == userId && user.loginStatus == 'Online'){
                    populateCallInformation(user, "CALLEE");
                    Socket.emit('callRequest', userId);
                } else if(user.id == userId && user.loginStatus == 'Offline'){
                    alert(user.name+' is offline...');
                }
            });
        }, user);
    }

    populateCallInformation = function(callParty, callPartyType){
       //Add a call notification to party
       if($scope.calls.length < 4){
           if($scope.calls.length == 0){
               callParty.type = 'success';
           } else{
               callParty.type = 'warning';
           }

	   if(callPartyType == "CALLER"){
               callParty.message = callParty.name + " is calling";
           } else{
               callParty.message = "Calling " + callParty.name + "\n Please wait...";
           }

           callParty.partyType = callPartyType;
           $scope.calls.push(callParty);
       } else{
          //Excess number of calls to a person.. 
       }
    }

    dePopulateCallInformation = function(index){
        $scope.calls.splice(index, 1);
    }

    $scope.rejectCall = function(index, callerId) {
        //$scope.calls.splice(index, 1);
        dePopulateCallInformation(index);
        console.log("Caller is: "+callerId);
        var callAnswerDetails = {
            callerId: callerId,
            answer:{ 
                     status: 'rejected'
            }
        }
        Socket.emit('callAnswer', callAnswerDetails);
    };

    $scope.answerCall = function(index, callerId){
        checkIfCameraOnOperate(function(index, callerId){
            $scope.onCallFriend = callerId;//Friend who is connected on the other side.
            console.log('On Call Friend: ', $scope.onCallFriend);
            //$scope.calls.splice(index, 1);
            dePopulateCallInformation(index);
            console.log("Answering...");
            var roomName = createRoomName(10);
            var callAnswerDetails = {
                callerId: callerId,
                answer:{ 
                         status: 'picked',
                         roomName: roomName
                }
            }
            Socket.emit('callAnswer', callAnswerDetails);
            console.log(roomName);
            Webrtc.joinRoom(roomName);
        } ,index, callerId);
    }

    createRoomName = function(nameSize){
        var roomName = "";
        var possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
        for( var i=0; i < nameSize; i++ )
        {
            roomName += possibleLetters.charAt(Math.floor(Math.random() * possibleLetters.length));
        }
        return roomName;
    }
}]);
