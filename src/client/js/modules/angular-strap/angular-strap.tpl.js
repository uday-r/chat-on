/**
 * angular-strap
 * @version v2.0.0 - 2014-04-07
 * @link http://mgcrea.github.io/angular-strap
 * @author Olivier Louvignes (olivier@mg-crea.com)
 * @license MIT License, http://www.opensource.org/licenses/MIT
 */
(function(window, document, undefined) {
'use strict';

// Source: /Users/olivier/Dropbox/Projects/angular-strap/src/alert/alert.tpl.js
// Source: /Users/olivier/Dropbox/Projects/angular-strap/src/tooltip/tooltip.tpl.js
angular.module('mgcrea.ngStrap.tooltip').run([
  '$templateCache',
  function ($templateCache) {
    $templateCache.put('tooltip/tooltip.tpl.html', '<div class="tooltip in" ng-show="title"><div class="tooltip-arrow"></div><div class="tooltip-inner" ng-bind="title"></div></div>');
  }
]);
})(window, document);
