angular.module('Chat-On')
.controller("AccountController", ['$rootScope', '$scope', '$modal', 'SDK_AUTH_CONSTANTS', function($rootScope, $scope, $modal, SDK_AUTH_CONSTANTS){

    var userDetailsLoaded;
    var friendsLoaded;

    $scope.dropDown = [
        {
         'text':'Account Settings',
         click:''
        },
        {
         text:'Logout',
         click:'logout()'
        }
    ];

    $scope.loginPanelSelection = 'Login'

    $scope.logout = function(){
        console.log('Clicked logout button');

        //Restore the title name of the login module.
        $scope.loginPanelSelection = 'Login';

        $scope.user.loginStatus = 'Logout';
        $scope.user.name = null;
        $scope.user.id = null;
        $scope.user.loginType = null;
        $scope.user.pictureDetails = null;
        $scope.contacts.length = 0;
    }

    $scope.user = {};
    $scope.waiting = {};
    $scope.register = {name : null};

    $scope.contacts = [];
    $scope.title = 'Login and Chat';
    $scope.content = 'FB Button';

    $scope.loginPopup = $modal({scope: $scope,
                                template: 'templates/LoginPopup.html',
                                animation: "am-fade-X", 
                                show: false, 
                                backdrop: 'static', 
                                keyboard: false});
  
    $scope.loginPopup.$promise.then(function(){
        $scope.$watch('user', function(newValue, oldValue){
            if($scope.user.name != null){
                userDetailsLoaded = true;
            } else{
                userDetailsLoaded = false;
            }
            toggleLoginPopup();
        }, true);
     
        $scope.$watch('contacts', function(newValue){
            if($scope.contacts.length > 0){
                friendsLoaded = true;
            } else{
                friendsLoaded = false;
            }
            toggleLoginPopup();
        }, true);
     
        toggleLoginPopup = function(){
            if(userDetailsLoaded == true && friendsLoaded == true){
                $scope.loginPopup.hide();
            } else if(userDetailsLoaded == false && friendsLoaded == false){
                //Add this under else if...........
                $scope.loginPopup.show();
            }
        }

        $scope.facebookLogin = function(){
            $scope.loginPanelSelection = 'Waiting';
            $scope.waiting.message="Fetching you details from Facebook...";
            facebookLoginAndDetails();
        }
    });

    facebookLoginAndDetails = function(){
        var options = SDK_AUTH_CONSTANTS['Facebook'];
        FB.init(options);
        FB.getLoginStatus(function(loginData){
            //$rootScope.$broadcast('login.data', loginData);
            if(loginData.status == 'unknown' || loginData.status == 'not_authorized'){
                FB.login();
                FB.Event.subscribe('auth.authResponseChange', function(response) {
                    if(response.status == 'connected'){
                        accessFacebookDetails();
                    }
                });
            } else{
                accessFacebookDetails();
            }
        });
    }

    accessFacebookDetails = function(){
        $scope.waiting.message="Fetching your details from Facebook...";
        var user = {};
        FB.api('me', function(profileDetails){
            $rootScope.$apply(function(){
                console.log(profileDetails.name);
                user.loginStatus = 'Online';
                user.name = profileDetails.name;
                user.id = profileDetails.username;
                user.loginType = 'Facebook';
            });

            $scope.waiting.message="Fetching your picture from Facebook...";
            FB.api('me/picture', function(pictureDetails){
                $rootScope.$apply(function(){
                    user.picture = pictureDetails.data.url;
                    $scope.user = user;
                });
            });
        }); 
        fetchContacts();
    }

    fetchContacts = function(){
        $scope.waiting.message="Fetching your contacts from Facebook";
        FB.api('/me/friends', {fields: 'name, username, id, location, birthday'}, function(response) {
            friends = response.data;
            var friendIndex = 0; 
            friends.forEach(function(friend){
                FB.api(friend.id+'/picture', function(pictureDetails){
                    friend.picture = pictureDetails.data.url;
                    friend.loginStatus = 'Offline';
                    friend.loginType = 'Facebook';
                    if(friend.username != undefined){
                        friend.id = friend.username;
                    }
                    if(friends.length == friendIndex+1){
                        $rootScope.$apply(function(){
                            $scope.contacts = $scope.contacts.concat(friends);
                            $scope.waiting.message=null;
                        });
                    }
                    friendIndex = friendIndex + 1;
                });
            });
        });
    }
}]);
