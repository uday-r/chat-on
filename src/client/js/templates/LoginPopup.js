angular.module("VideoChatTemplates").run(["$templateCache", function($templateCache) {
    $templateCache.put("templates/LoginPopup.html",
    '<div class="modal" tabindex="-1" role="dialog">' +
      '<div class="modal-dialog">' +
        '<div class="modal-content">' +
          '<div class="modal-header" ng-show="title">' +
            '<h4 class="modal-title" ng-bind="title"></h4>' +
          '</div>' +
          '<div class="modal-body" ng-switch on="loginPanelSelection">' +
              '<div ng-switch-when="Login">' +
                  '<div class="row">' +
                      '<div class="col-md-12"><!-- or-spacer-vertical right -->' +
                          '<button class="btn-social button-Facebook" ng-click=\'facebookLogin("Login")\' >Facebook Sign-In</button>' +
                      '</div>' +
                      /*'<div class="col-md-5" style="width:45%"><!-- or-spacer-vertical right -->' +
                          '<button class="btn-social button-Facebook" ng-click=\'facebookLogin("Login")\' >Facebook Sign-In</button>' +
                          '<button class="btn-social button-Gmail" id="google-login-button">Google Sign-In</button>' +
                      '</div>' +
                      '<div class="mask"></div>' +
                      '<div class="col-md-6" style="width:53%">' +
                          '<input type="text" class="form-control custom-input" placeholder="Type in your name" ng-model=\'registerDetails.name\'>' +
                          '<button class="btn btn-secondary form-control custom-input" style="margin-top:30px"' + 
                          'ng-click=\'nativeLogin()\' >SIGN-IN</button>' +
                      '</div>' +*/
                  '</div>' +
              '</div>' +
              '<div ng-switch-when="Waiting">' +
                  '<img class="waiting-animation" src="images/animations/waiting.jpg">' +
                  '<label class="waiting-message">{{waiting.message}}</label>' +
              '</div>' +
          '</div>' +
        '</div>' +
      '</div>' +
    '</div>');
}]);
