var setIoSocketConnection = function(io){
    io.sockets.on('connection', function (client) {

        client.on('messageToServer', function (message) {
            client.broadcast.emit("messageToClient", message);
        });

        client.on('join', function(clientDetails){
            try{
                addClient(client, clientDetails)
            } catch (error){
                //Understanding in exceptions required
                console.log(error);
                client.emit(constants.error,{'message': 'No name found'});
            }
        });

        client.on('registerFriends', function(friends){
            console.log('Entered register friends event handler');
            try{
                console.log('Friends length: ', friends.length);
                if(friends.length > 0){ 
                    clientList.registerFriends(client.uniqueId, friends, function(updatedFriends){
                        console.log('Firing updating events....');
                        client.emit('updatedFriendsList', updatedFriends);
                    });
                } else {
                    console.log('No contacts found to filter....');
                }
            } catch(error){
                console.log('Handeled in registerFriends event handler...');
                console.log(error);
            }
        });

        client.on('endCall', function(call){
            console.log('Termination...............');
            console.log('Terminator: ', call.terminator);
            if(call.terminator){
                client.emit('leaveWebrtcRoom', {terminator: false});
            } else{
                var callParticipantSocket = clientList.getUserSocketByUserId(client.callParticipant).data;
                var chatRoom = clientList.getGroups(client.callParticipant).pop();
                callParticipantSocket.emit('leaveWebrtcRoom', null);
            }
        });

        client.on('leave', function(){
            console.log('Leave socket initiated...');
            if(!removeClient(client)){
                //client.emit(constants.error, {message: "You are doing something you are not supposed to do."});
            } else{
                //client.emit('SERVERMESSAGES', {message: "LOgged out"});
            }
        });

        client.on('disconnect', function(){
            console.log('Socket disconnected.....');
            removeClient(client);
        })

        client.on('callRequest', function(calleeId){
            callerId = client.uniqueId;
            callClient(callerId, calleeId, "callee", function(callerId, calleeId, calleeSocketId){
                if(io.sockets.sockets[calleeSocketId] == null){
                    console.log('Error : Callee not found.');
                    client.emit('error', {'message': clientList.getUserName(calleeId).data + ' has gone offline.'});
                } else if(io.sockets.sockets[calleeSocketId].uniqueId==calleeId){
                    console.log('Client Id: ', calleeSocketId);
                    var calleeNameData = clientList.getUserName(calleeId);
                    var callerNameData = clientList.getUserName(callerId);
                    if(callerNameData.status == constants.success && calleeNameData.status == constants.success){
                        io.sockets.sockets[calleeSocketId].emit("incomingCall", { 
                            caller:{
                                name:callerNameData.data,
                                id:callerId
                            },
                            callee:{
                               name:calleeNameData.data,
                               id:calleeId
                            }});
                    } else {
                        //client has gone offline error message to the caller
                        console.log('Error either of them is offline');
                    }
                }
            });
        });

        client.on('callAnswer', function(callAnswerDetails){
            var callerId = callAnswerDetails.callerId;
            var calleeId = client.uniqueId;
            console.log("Entered Call Answer block.");
            answer = callAnswerDetails.answer;
            callClient(callerId, calleeId, "caller", function(callerId, calleeId, callerSocketId){
                console.log('Start......', io.sockets.sockets[callerSocketId].uniqueId);
                console.log('Caller socket Id: ', callerSocketId);
                console.log('Compare.... caller: ', callerId);
                console.log('Compare..... callee: ', calleeId);
                if(io.sockets.sockets[callerSocketId].uniqueId==callerId){
                    console.log('Entered answer block');
                    client.callParticipant = callerId;
                    io.sockets.sockets[callerSocketId].callParticipant = calleeId;
                    clientList.addGroup(calleeId, answer.roomName);
                    clientList.addGroup(callerId, answer.roomName);
                    io.sockets.sockets[callerSocketId].emit("acknowledgeCallee", { callee: calleeId, message: answer});
                }
                console.log('End........');
            });
        });
    });
}

module.exports = setIoSocketConnection;

require("./ClientSocketPool.js");
var constants = require("./Constants.json");
var clientList = new ClientNameList();

addClient = function(clientSocket, clientDetails){
    console.log('addClient function in server.js');
    console.log('Client details:', clientDetails);
    if(clientDetails.id == '' || clientDetails.id == undefined){
        var noNameError = new Error('Please enter your name or Login with Facebook or Google.');
        noNameError.type = 'No name';
        throw noNameError;
    } else { 
        console.log('calling addClient of socket pool..');
        clientSocket.uniqueId = clientDetails.id;
        clientList.addClient(clientDetails,clientSocket,addSocketEventListener);
    }
}

removeClient = function(clientSocket){
    return clientList.removeClient(clientSocket, removeSocketEventListener);
}   
    
addSocketEventListener = function(clientInformation){
    console.log('clientInformation is ', clientInformation);
    var clientIndex = clientInformation.index;
    var clientDetails = clientInformation.clientDetails;
    var friendsInformation = clientList.getUserFriends(clientDetails);
    console.log('Friends information in addSocketEventListener (server.js): ',friendsInformation);
    var friendSocket;
    if(friendsInformation.status == constants.success){
        var friendsIndexArray = friendsInformation.data;
        console.log('Data: ', friendsIndexArray);
        friendsIndexArray.forEach(function(friendIndex){
            friendSocket = clientList.getUserSocketByIndex(friendIndex);
            console.log('Friend Socket:  ', friendSocket);
            console.log(friendSocket == "socket");
            console.log(friendSocket == 'socket');
            if(friendSocket != "socket"){ 
                console.log('Entered the if condition with ', friendSocket);
                friendSocket.emit("addedNewUser", clientDetails);
            }
        });
    }
}

getUserStatus = function(userUniqueId){
    var rooms = clientList.getGroups(userUniqueId);
    if(rooms.length > 1){
        return 'Busy';
    } else{
        return 'Online';
    }
}

removeSocketEventListener = function(client, groups, clientDetails){
    groups.forEach(function(group){
        client.leave(group);
    });
    client.broadcast.emit("userOffline", clientDetails);
    console.log('Removing completed....');
}

callClient = function(callerId, calleeId, client, message){
    var calleeSocketDetails = clientList.getUserSocketByUserId(calleeId);
    var callerSocketDetails = clientList.getUserSocketByUserId(callerId);

    /*console.log('Callee socket details: ', calleeSocketDetails.data.id);
    console.log('Caller socket details: ', callerSocketDetails.data.id);*/
        //iterate through the sockets to check the current loginStatus of the socket client (if he is online).
    if(client == constants.callee && calleeSocketDetails.status != constants.error) {
        message(callerId, calleeId, calleeSocketDetails.data.id);
    } else if(client == constants.caller && callerSocketDetails.status != constants.error) {
        message(callerId, calleeId, callerSocketDetails.data.id);
    }
}

getOnlineUsersList = function(currentUserId){
    //get all users except of the current user.
    var onlineUsersList = clientList.getList();
    var fellowUsers = [];
    onlineUsersList.every(function(user){
        console.log('User: ', user);
        if(user.id != currentUserId){
            fellowUsers.push(user);
        }
        return true;
    });
    return fellowUsers;
}

