var express = require('express');
var app = express();
var server = app.listen(process.env.PORT || 8088);
var io = require('socket.io').listen(server);

app.use(express.static(__dirname + '/client/'));

var socketConnection = require("./SocketConnection");
socketConnection(io);
