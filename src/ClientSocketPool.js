var constants = require("./Constants.json");
ClientNameList = function(){

    this._listSocketIndex = new Array();
    this._listDetails = new Array();
    this._listIndex = new Array();
    var self = this;
    this.addClient = function(clientDetails, clientSocket, addSocketEventListener){
        console.log('Entered addClient in socket pool...');
        var clientUniqueId = clientDetails.id;
        var loginType = clientDetails.loginType;
        var userIndex = self._listIndex.indexOf(clientUniqueId);
        var userInformation;
        console.log('The userIndex of '+ clientUniqueId + ' is ' + userIndex);
        console.log('loginType is ' + loginType + ' userIndex is ' + userIndex);
        if(userIndex != -1 && loginType != 'Facebook'){
            //Native Login for the application.
            clientUniqueId = self.createUniqueUserName(constants.uniqueNameSize);
            userInformation = self._addClientDetails(null, clientDetails, clientSocket);
            //console.log('Name: ', clientUniqueId);
        } else if(userIndex != -1){
            //User logs-in after the user is registered.
            console.log('Adding socket to the socket list...');
            console.log('The user is: ', self._listDetails[userIndex]);
            var socket = new Object({socket:clientSocket});
            self._listDetails[userIndex].loginStatus = clientDetails.loginStatus;
            self._listSocketIndex[userIndex] = socket;
            userInformation = {'index': userIndex, 'clientDetails': self._listDetails[userIndex]};
        } else if(userIndex == -1){
            //User logs in and he registers himself.
            userInformation = self._addClientDetails(null, clientDetails, clientSocket);
        }
        console.log('Triggering the addSocketEventListener with userInformation ', userInformation );
        addSocketEventListener(userInformation);
    }

    this._addClientDetails = function(friendIndex, clientDetails, clientSocket){
        console.log('_addClientDetails function in socket pool...');
        if(friendIndex != null){
            if( typeof clientDetails.friends == 'undefined' || clientDetails.friends.length == 0){
                clientDetails.friends = [];
            }
            clientDetails.friends.push(friendIndex);
        }

        console.log('Registering friend: ', clientDetails);
        var groupArray = new Array("Online");
        var clientUniqueId = clientDetails.id;
        clientDetails.groups = groupArray;
        var socket = new Object({socket:clientSocket});
        self._listSocketIndex.push(socket);
        self._listDetails.push(clientDetails);
        var index = self._listIndex.push(clientUniqueId);
        console.log('Index of added value', index); 
        console.log('clientUniqueIds...\': ', self._listIndex);
        console.log('Entered the clientDetails to the socket pool...\n Here are the changes...');
        console.log('Client details list length: ', self._listDetails.length);
        console.log('Sockets list length: ', self._listSocketIndex.length);
        console.log('Indexes list length: ', self._listIndex.length);
        return { 'index': --index, 'clientDetails': clientDetails};
    }

    this.removeClient = function(clientSocket, removeSocketEventListener){
        var clientUniqueId = clientSocket.uniqueId;
        console.log('Unique ID: ', clientUniqueId);
        //console.log(' PRE-Indexes list: ', self._listIndex);
        var clientIndex = self._listIndex.indexOf(clientUniqueId);
        console.log('index: ', clientIndex);
        console.log('Unique ID: ', clientUniqueId);
        console.log('Deleting function:');

        //console.log('Indexes list: ', self._listIndex);
        if(clientIndex != -1){
            var friendsIndexArray = self._listDetails[clientIndex].friends;
            console.log('Friends: ', friendsIndexArray);

            var clientRooms = self._listDetails[clientIndex].groups;
            var clientDetails = self._listDetails[clientIndex];
            
            self._listDetails[clientIndex].loginStatus = 'Offline';
            //self._listSocketIndex[clientIndex] = undefined;
            //self._listIndex[clientIndex] = undefined;
            removeSocketEventListener(clientSocket, clientRooms, clientDetails);

            friendsIndexArray.forEach(function(friendIndex){
                console.log('Length of list details: ', self._listDetails.length);
                var friendDetails = self._listDetails[friendIndex];
                console.log('All details', friendDetails);
                var loginStatus = friendDetails.loginStatus;
                console.log('Login status: ', loginStatus);
                console.log('Fellow contact name: ', friendDetails.name);
                console.log('Fellow contact friends\' list: ', friendDetails.friends);
                var totalContacts = friendDetails.friends.length;
                if(totalContacts == 1 && loginStatus == 'Offline'){
                    console.log('Removing from all lists', friendDetails.name);
                    self._listDetails[friendIndex].loginStatus = 'Offline';
                    //self._listSocketIndex[friendIndex] = undefined;
                    //self._listIndex[friendIndex] = undefined;
                }
                if(loginStatus == 'Online'){
                    var socket = self._listSocketIndex[friendIndex].socket;
                    if(socket != 'socket'){
                        console.log('Socket is: ',socket);
                        socket.emit('friendUpdated', clientDetails);
                    }
                }
                console.log('\n');
            });

            console.log('Client details list length: ', self._listDetails.length);
            console.log('Sockets list length: ', self._listSocketIndex.length);
            console.log('Indexes list length: ', self._listIndex.length);

            return true;
        } else{
            return false;
        }
    }

    this.addGroup = function(clientUniqueId, roomName){
        var clientSocket = self.getUserSocketByUserId(clientUniqueId).data;
        var userDetails = self.getUserDetails(clientUniqueId).data;
        console.log(userDetails);
        userDetails.groups.push(roomName);
        self.setUserDetails(clientUniqueId, userDetails);
        clientSocket.join(roomName);
        /*var clientIndex = this._listIndex.indexOf(clientId);
        this._listSocketIndex[clientIndex].socket.groups.push(roomName);*/
    }

    this.removeGroup = function(clientSocket, roomName){
        var clientName = clientSocket.uniqueId;
        var clientIndex = self._listIndex.indexOf(clientId);
        self._listSocketIndex[clientIndex].socket.groups.pop(roomName);
        client.leave(roomName);
    }

    /*This function takes the contacts and appends to the friends but only returns 
         just contacts which were initially sent to the server. */
    this.registerFriends = function(clientUniqueId, friends, registerFriendsCallback){
        var friendsIndexArray = [];
        //The reason for this name is the context here this variable represents the array of friend's index number.
        var clientArrayIndex = self._listIndex.indexOf(clientUniqueId);
        friends.forEach(function(friend, friendArrayIndex){
            var friendUniqueId = friend.id;
            var friendIndex;
            var friendIdInformation = self.getUserIndex(friendUniqueId);
            if(friendIdInformation.status == constants.success){
                friendIndex = friendIdInformation.data;
                var friendDetails = self._listDetails[friendIndex];
                if(friendDetails.friends.indexOf(clientArrayIndex) == -1){
                    friendDetails.friends.push(clientArrayIndex);
                }
                friends[friendArrayIndex] = self._listDetails[friendIndex];
            } else{
                var returnedUser = self._addClientDetails(clientArrayIndex, friend, 'socket');
            }
            friendsIndexArray.push(self.getUserIndex(friendUniqueId).data); 

            if(friendArrayIndex+1 == friends.length){
                //callback
                registerFriendsCallback(friends);
                try{
                    self._addFriends(clientUniqueId, friendsIndexArray);
                } catch(error){
                    throw error;
                }
            }
        });
    }

    this.getGroups = function(clientUniqueId){
        var clientIndex = self._listIndex.indexOf(clientUniqueId);
        return self._listDetails[clientIndex].groups;
    }

    this._addFriends = function(clientUniqueId, friendsIndexArray){
        console.log('Client unique Id is: ', clientUniqueId);
        var clientIndex = self._listIndex.indexOf(clientUniqueId);
        console.log('Client Index is: ', clientIndex);
        if(clientIndex != -1){
            if(self._listDetails[clientIndex].friends == undefined || self._listDetails[clientIndex].length == 0){
                self._listDetails[clientIndex].friends = friendsIndexArray;
            } else {
                self._listDetails[clientIndex].friends = self._listDetails[clientIndex].friends.concat(friendsIndexArray);
            }
        } else{
            var userNotFoundError = new Error('User with clientUniqueId '+ clientUniqueId +' not found');
            throw userNotFoundError;
        }
    }

    this.getList = function(){
        return self._listDetails;
    }

    this.getUserIndex = function(clientUniqueId){
        var userIndex = self._listIndex.indexOf(clientUniqueId);
        if(userIndex != -1){
            return { status:constants.success, data:userIndex};
        } else{
            return { status:constants.error, message:'User not found'};
        }
    }

    this.getUserName = function(clientUniqueId){
        var userIndex = self._listIndex.indexOf(clientUniqueId);
        if(userIndex != -1){
            return { status:constants.success, data:self._listDetails[userIndex].name };
        } else{
            return { status:constants.error, message:'User offline (in getUserName function)'};
        }
    }

    this.getUserSocketByUserId = function(clientUniqueId){
        console.log('ID for socket: ', clientUniqueId);
        var userIndex = self._listIndex.indexOf(clientUniqueId);
        if(userIndex != -1){
            return { status:constants.success, data:self._listSocketIndex[userIndex].socket };
        } else{
            return { status:constants.error, message:'User offline (in getUserSocketByUserId function)'};
        }
    }

    this.getUserSocketByIndex = function(clientIndex){
        return self._listSocketIndex[clientIndex].socket;
    }

    this.getUserDetails = function(clientUniqueId){
        var clientIndex = self._listIndex.indexOf(clientUniqueId);
        if(clientIndex != -1){
            return { status:constants.success, data:self._listDetails[clientIndex] };
        } else{
            return { status:constants.error, message:'User offline (in getUserDetails function)' };
        }
    }

    this.seekUserFriends = function(clientIndex){
        if(typeof self._listDetails[clientIndex].friends ==  'undefined'){
            return { status:constants.error, message:'User offline (in seekUserFriends function)' };
        } else{
            return { status:constants.success, data: self._listDetails[clientIndex].friends };
        }
    }

    this.getUserFriends = function(userDetails){
        console.log("Get user's friends in CLientSocketPool ", userDetails);
        if(typeof userDetails.friends ==  'undefined'){
            return { status:constants.error, message:'User dosent have friends' };
        } else{
            return { status:constants.success, data: userDetails.friends };
        }
    }

    this.getUserLoginStatus = function(clientIndex){
        if(clientIndex != -1){
            return { status:constants.success, data:self._listDetails[clientIndex].loginStatus };
        } else{
            return { status:constants.error, message:'User offline (in getUserLoginStatus function)'};
        }
    }

    this.setUserDetails = function(clientUniqueId, clientDetails){
        var userIndex = self._listIndex.indexOf(clientUniqueId);
        if(userIndex != -1){
            self._listDetails[userIndex] = clientDetails;
            return true;
        } else{
            return false;
        }
    }

    this.createUniqueUserName = function(nameSize){
        var uniqueName = "";
        var possibleLetters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
 
        for( var i=0; i < nameSize; i++ )
        {
            uniqueName += possibleLetters.charAt(Math.floor(Math.random() * possibleLetters.length));
        }
 
        return uniqueName;
    }
}
